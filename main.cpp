#include <iostream>
#include <algorithm>

template <typename T>
void quickSort(T begin, T end) {
	if(begin == end)return;
	if(begin + 1 == end)return;

	T pivot_first = begin;
	T pivot_last = begin + 1;
	T it = begin + 1;
	while(it < end) {
		if(*it == *pivot_first) {
			std::swap(*it, *pivot_last);
			++pivot_last;
		} else if(*it < *pivot_first) {
			std::swap(*it, *pivot_first);
			std::swap(*it, *pivot_last);
			++pivot_first;
			++pivot_last;
		} else {
		}
		++it;
	}
	quickSort(begin, pivot_first);
	quickSort(pivot_last, end);

}

int tab[15] = {12, 1, 3, 5, 9, 8, 0, -4, -6, 7, 2, 11, 4, 6, 8};
int main() {
	/*for (int i = 0; i < 10; ++ i) {
		std::cin>>tab[i];
	}*/
	quickSort(tab, tab+15);
	for (int i = 0; i < 15; ++ i) {
		std::cout<<tab[i]<<" ";
	}
	return 0;
}
